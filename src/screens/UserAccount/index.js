import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "../../components";
import { storeUserData } from "../../actions";
import UserBetHistoryList from "./UserBetHistoryList";
import { getUserDetails, addMoney, withdrawMoney } from "../../services/user";

import "./UserAccount.css";
import MoneyModal from "./modals/MoneyModal";
import PaymentHistoryModal from "./modals/PaymentHistoryModal";

const mapStateToProps = ({ user }) => {
  return { user };
};

const mapDispatchToProps = dispatch => {
  return {
    storeUserData: userData => dispatch(storeUserData(userData))
  };
};

class UserAccountComponent extends Component {
  state = {
    showAddMoneyModal: false,
    showWithdrawMoneyModal: false,
    showCreditHistoryModal: false
  };

  componentDidMount() {
    this.loadData();
  }

  async loadData() {
    try {
      const { user } = this.props;
      // yes, we are querying by name, do not ask any questions
      const userData = await getUserDetails(user.name);
      this.props.storeUserData(userData);
    } catch (err) {
      console.warn(err);
    }
  }

  addMoney = async amount => {
    this.closeAddMoneyModal();
    if (amount > 0) {
      const accountBalance = await addMoney(this.props.user.name, amount);
      this.props.storeUserData({ accountBalance });
    }
  };

  showAddMoneyModal = () => this.setState({ showAddMoneyModal: true });

  closeAddMoneyModal = () => this.setState({ showAddMoneyModal: false });

  withdrawMoney = async amount => {
    this.closeWithdrawMoneyModal();
    // I trust no one, especially backend
    if (amount > 0 && amount < (this.props.user.accountBalance || 0)) {
      const accountBalance = await withdrawMoney(this.props.user.name, amount);
      this.props.storeUserData({ accountBalance });
    }
  };

  showWithdrawMoneyModal = () =>
    this.setState({ showWithdrawMoneyModal: true });

  closeWithdrawMoneyModal = () =>
    this.setState({ showWithdrawMoneyModal: false });

  showCreditHistoryModal = () =>
    this.setState({ showCreditHistoryModal: true });

  closeCreditHistoryModal = () =>
    this.setState({ showCreditHistoryModal: false });

  render() {
    if (!this.props.user) {
      return null;
    }
    const { name, accountBalance, paymentList } = this.props.user;
    const {
      showAddMoneyModal,
      showWithdrawMoneyModal,
      showCreditHistoryModal
    } = this.state;
    return (
      <div className="UserAccount">
        <h2>Your Account:</h2>
        <div className="UserAccountData">
          <span>{`Login: ${name}`}</span>
          <span>{`Your Account Balance: ${accountBalance || 0}$`}</span>
        </div>
        <div className="UserAccountButton">
          <Button onClick={this.showAddMoneyModal} text="Add credit" />
        </div>
        <div className="UserAccountButton">
          <Button
            onClick={this.showWithdrawMoneyModal}
            text="Withdraw credit"
          />
        </div>
        <div className="UserAccountButton">
          <Button
            onClick={this.showCreditHistoryModal}
            text="See credit history"
          />
        </div>
        <h2>Bet History:</h2>
        <UserBetHistoryList />
        <MoneyModal
          titleText="Add Money"
          shouldBeDisplayed={showAddMoneyModal}
          onClose={this.closeAddMoneyModal}
          onConfirm={this.addMoney}
        />
        <MoneyModal
          titleText="Withdraw Money"
          shouldBeDisplayed={showWithdrawMoneyModal}
          onClose={this.closeWithdrawMoneyModal}
          onConfirm={this.withdrawMoney}
        />
        <PaymentHistoryModal
          shouldBeDisplayed={showCreditHistoryModal}
          onClose={this.closeCreditHistoryModal}
          paymentHistory={paymentList}
        />
      </div>
    );
  }
}

const UserAccount = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAccountComponent);

export default UserAccount;
