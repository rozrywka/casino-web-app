import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import BetSlipModal from "./modals/BetSlipModal";

const styles = theme => ({
  root: {
    width: "100%",
    maxWidth: 450,
    backgroundColor: theme.palette.background.paper
  }
});

const mapStateToProps = state => {
  const { betHistory } = state.user;
  return { betHistory };
};

class BetHistoryList extends Component {
  state = {
    betSlipToDisplay: null
  };

  render() {
    const { classes, betHistory } = this.props;
    const { betSlipToDisplay } = this.state;

    if (!betHistory) {
      // display sth, maybe also loading indicator
      return null;
    }

    return (
      <div className={classes.root}>
        <List component="nav">
          {betHistory.map((betSlip, index) => {
            const {
              betSlipId,
              betSlipStatus,
              purchasedDate,
              collectedDate
            } = betSlip;
            return (
              <div key={`${index}`}>
                <ListItem
                  button
                  onClick={() => {
                    this.setState({ betSlipToDisplay: betSlip });
                  }}
                >
                  <ListItemText
                    primary={`Bet Slip ${betSlipId} – ${betSlipStatus}`}
                    secondary={`Purchased: ${purchasedDate} Collected: ${
                      collectedDate ? collectedDate : "Not yet"
                    }`}
                  />
                </ListItem>
                <Divider />
              </div>
            );
          })}
        </List>
        <BetSlipModal
          shouldBeDisplayed={!!betSlipToDisplay}
          betSlipData={betSlipToDisplay}
          onClose={() => this.setState({ betSlipToDisplay: null })}
          buttonText="Ok"
        />
      </div>
    );
  }
}

const UserBetHistoryList = withStyles(styles)(
  connect(mapStateToProps)(BetHistoryList)
);

export default UserBetHistoryList;
