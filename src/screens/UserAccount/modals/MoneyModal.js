import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Input from "@material-ui/core/Input";
import DialogTitle from "@material-ui/core/DialogTitle";

// add or withdraw money modal
export default class MoneyModal extends Component {
  state = { amount: 0 };

  resetState = () => {
    this.setState({ amount: 0 });
  };

  onConfirm = () => {
    this.props.onConfirm(this.state.amount);
    this.resetState();
  };

  onClose = () => {
    this.props.onClose();
    this.resetState();
  };

  render() {
    const {
      shouldBeDisplayed, // remember to change this to false in onClose
      titleText
    } = this.props;
    return (
      <div>
        <Dialog open={shouldBeDisplayed} onClose={this.onClose}>
          <DialogTitle>{titleText}</DialogTitle>
          <DialogContent>
            <DialogContentText>How much?</DialogContentText>
            <Input
              type="number"
              value={this.state.amount}
              onChange={event => this.setState({ amount: event.target.value })}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onClose} color="secondary" autoFocus>
              Cancel
            </Button>
            <Button onClick={this.onConfirm} color="primary" autoFocus>
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
