import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Cancel from "@material-ui/icons/Cancel";

export default class PaymentHistoryModal extends Component {
  render() {
    const {
      shouldBeDisplayed, // remember to change this to false in onClose
      onClose,
      paymentHistory
    } = this.props;

    return (
      <div>
        <Dialog open={shouldBeDisplayed} onClose={onClose}>
          <DialogActions>
            <DialogTitle>Credit History</DialogTitle>
            <Button onClick={onClose}>
              <Cancel style={{ cursor: "pointer" }} color="primary" />
            </Button>
          </DialogActions>
          <DialogContent>
            {paymentHistory &&
              paymentHistory.map(
                ({ paymentDate, amount, paymentType }, idx) => (
                  <DialogContentText
                    key={`${idx}`}
                  >{`${paymentType} ${amount}$ on ${paymentDate}`}</DialogContentText>
                )
              )}
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}
