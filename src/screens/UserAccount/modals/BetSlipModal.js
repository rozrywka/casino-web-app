import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export default class BetSlipModal extends Component {
  render() {
    const {
      shouldBeDisplayed, // remember to change this to false in onClose
      onClose,
      betSlipData,
      buttonText
    } = this.props;

    return (
      <div>
        <Dialog open={shouldBeDisplayed} onClose={onClose}>
          <DialogTitle>
            {betSlipData
              ? `Bet Slip ${betSlipData.betSlipId} – ${
                  betSlipData.betSlipStatus
                }`
              : ""}
          </DialogTitle>
          <DialogContent>
            {betSlipData &&
              betSlipData.betList.map(({ amount, bettingOdd }, idx) => (
                <DialogContentText
                  key={`${idx}`}
                >{`Amount: ${amount}, Odd: ${bettingOdd}`}</DialogContentText>
              ))}
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} color="primary" autoFocus>
              {buttonText}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
