import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Cancel from "@material-ui/icons/Cancel";

export default class SummaryModal extends Component {
  totalAmount = () => {
    let total = 0;
    this.props.cart.forEach(({ id, amount }) => { total += parseInt(amount) });
    return total;
  };

  render() {
    const {
      shouldBeDisplayed, // remember to change this to false in onClose
      onClose,
      onConfirm
    } = this.props;

    return (
      <div>
        <Dialog open={shouldBeDisplayed} onClose={onClose}>
          <DialogActions>
            <DialogTitle>Bets Summary</DialogTitle>
            <Button onClick={onClose}>
              <Cancel style={{ cursor: "pointer" }} color="primary" />
            </Button>
          </DialogActions>
          <DialogContent>
            <DialogContentText>{`Total: ${this.totalAmount()}`}</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} color="secondary" autoFocus>
              Cancel
            </Button>
            <Button onClick={onConfirm} color="primary" autoFocus>
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
