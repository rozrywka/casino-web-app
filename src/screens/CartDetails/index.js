import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { Button as CustomButton } from "../../components";
import Button from "@material-ui/core/Button";
import Cancel from "@material-ui/icons/Cancel";
import connect from "react-redux/es/connect/connect";
import { cartClearItems, cartRemoveItem } from "../../actions";
import SummaryModal from "./modals/SummaryModal";
import "./CartDetails.css";

const mapStateToProps = state => {
  return { betList: state.betList, cart: state.cart };
};

const mapDispatchToProps = dispatch => {
  return {
    removeFromCart: id => dispatch(cartRemoveItem(id)),
    clearCart: () => dispatch(cartClearItems())
  };
};

class CartDetailsComponent extends Component {
  state = {
    showSummaryModal: false,
  };

  showSummaryModal = () => this.setState({ showSummaryModal: true });
  closeSummaryModal = () => this.setState({ showSummaryModal: false });

  onConfirm = () => {
    console.log('Cart Confirmed!');
    this.props.clearCart();
    this.closeSummaryModal();
  };

  findBet(betId) {
    return this.props.betList.find(bet => bet.apiBetId === betId);
  }

  render() {
    const { cart, removeFromCart } = this.props;
    return (
      <div className="BetList" style={{ padding: 48 }}>
        <Grid container spacing={24}>
          <Grid item xs={7}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>Finished Date</TableCell>
                  <TableCell>Category</TableCell>
                  <TableCell>Result</TableCell>
                  <TableCell>Amount</TableCell>
                  <TableCell>Remove Bet</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {cart.map(({ id, amount }, i) => {
                  const bet = this.findBet(id);
                  return (
                    <TableRow key={i}>
                      <TableCell component="th" scope="row">
                        {bet.name}
                      </TableCell>
                      <TableCell>{bet.description}</TableCell>
                      <TableCell>{bet.finishedDate}</TableCell>
                      <TableCell>{bet.matchCategory}</TableCell>
                      <TableCell>{bet.matchResult}</TableCell>
                      <TableCell>{amount}</TableCell>
                      <TableCell>
                        <Button onClick={() => removeFromCart(id)}>
                          <Cancel
                            style={{ cursor: "pointer" }}
                            color="primary"
                          />
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
            <div className="SummaryButton">
              <CustomButton onClick={this.showSummaryModal} text="Summary" />
            </div>
          </Grid>
        </Grid>
        <SummaryModal
          shouldBeDisplayed={this.state.showSummaryModal}
          onClose={this.closeSummaryModal}
          onConfirm={this.onConfirm}
          cart={cart}
        />
      </div>
    );
  }
}

const CartDetails = connect(
  mapStateToProps,
  mapDispatchToProps
)(CartDetailsComponent);

export default CartDetails;
