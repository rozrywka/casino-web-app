import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Input from "@material-ui/core/Input";
import DialogTitle from "@material-ui/core/DialogTitle";

export default class BetAmountModal extends Component {
  state = { amount: 1 };

  resetState = () => {
    this.setState({ amount: 1 });
  };

  onConfirm = () => {
    this.props.onConfirm(this.state.amount);
    this.resetState();
  };

  onClose = () => {
    this.props.onClose();
    this.resetState();
  };

  render() {
    const {
      shouldBeDisplayed // remember to change this to false in onClose
    } = this.props;
    return (
      <div>
        <Dialog open={shouldBeDisplayed} onClose={this.onClose}>
          <DialogTitle>Choose bets number</DialogTitle>
          <DialogContent>
            <DialogContentText>How many bets?</DialogContentText>
            <Input
              type="number"
              value={this.state.amount}
              onChange={event => this.setState({ amount: event.target.value })}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onClose} color="secondary" autoFocus>
              Cancel
            </Button>
            <Button onClick={this.onConfirm} color="primary" autoFocus>
              Add to cart
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
