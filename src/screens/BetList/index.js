import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import { cartAddItem, storeBetList } from "../../actions";
import connect from "react-redux/es/connect/connect";
import { getBets } from "../../services/bet";
import BetAmountModal from "./BetAmountModal";

const mapStateToProps = state => {
  return { betList: state.betList };
};

const mapDispatchToProps = dispatch => {
  return {
    addToCart: (betId, amount) => dispatch(cartAddItem(betId, amount)),
    storeBetList: betList => dispatch(storeBetList(betList))
  };
};

class BetListComponent extends Component {
  state = {
    betIdToChooseAmountForOnModal: null
  };

  componentDidMount() {
    this.loadBets();
  }

  async loadBets() {
    try {
      const betList = await getBets();
      this.props.storeBetList(betList);
    } catch (err) {
      console.warn(err);
    }
  }

  addBetToCart = amountOfBets => {
    const betId = this.state.betIdToChooseAmountForOnModal;
    this.props.addToCart(betId, amountOfBets);
    this.closeBetAmountModal();
  };

  closeBetAmountModal = () => {
    this.setState({ betIdToChooseAmountForOnModal: null });
  };

  showChooseBetsAmountModal = id => {
    this.setState({ betIdToChooseAmountForOnModal: id });
  };

  render() {
    const { betList } = this.props;
    const { betIdToChooseAmountForOnModal } = this.state;
    if (!betList) {
      return null;
    }

    return (
      <div className="BetList" style={{ padding: 48 }}>
        <Grid container spacing={24}>
          <Grid item xs={6}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>Finished Date</TableCell>
                  <TableCell>Category</TableCell>
                  <TableCell>Result</TableCell>
                  <TableCell>Add Bet</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {betList.map(bet => {
                  return (
                    <TableRow key={bet.apiBetId}>
                      <TableCell component="th" scope="row">
                        {bet.name}
                      </TableCell>
                      <TableCell>{bet.description}</TableCell>
                      <TableCell>{bet.finishedDate}</TableCell>
                      <TableCell>{bet.matchCategory}</TableCell>
                      <TableCell>{bet.matchResult}</TableCell>
                      <TableCell>
                        <Button
                          onClick={() =>
                            this.showChooseBetsAmountModal(bet.apiBetId)
                          }
                        >
                          <AddShoppingCartIcon
                            style={{ cursor: "pointer" }}
                            color="primary"
                          />
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
        <BetAmountModal
          shouldBeDisplayed={!!betIdToChooseAmountForOnModal}
          onClose={this.closeBetAmountModal}
          onConfirm={this.addBetToCart}
        />
      </div>
    );
  }
}

const BetList = connect(
  mapStateToProps,
  mapDispatchToProps
)(BetListComponent);

export default BetList;
