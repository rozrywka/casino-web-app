import React, { Component } from "react";
import { TextField, Button, Spacer, ErrorDialog } from "../../components";
import {
  loginNameChange,
  loginEmailChange,
  loginPasswordChange,
  loginSwitchForm,
  loginError,
  loginErrorDialogClose,
  loginClearForm,
  saveAuthToken,
  storeUserData
} from "../../actions";
import { postLogin, postRegister } from "../../services/auth";

import "./Login.css";

import { connect } from "react-redux";

const mapStateToProps = state => {
  return { ...state.loginForm };
};

const mapDispatchToProps = dispatch => {
  return {
    onNameChange: name => {
      dispatch(loginNameChange(name));
    },
    onEmailChange: email => {
      dispatch(loginEmailChange(email));
    },
    onPasswordChange: password => {
      dispatch(loginPasswordChange(password));
    },
    onSwitchForm: () => {
      dispatch(loginSwitchForm());
    },
    onError: errorText => {
      dispatch(loginError(errorText));
    },
    onErrorDialogClose: () => {
      dispatch(loginErrorDialogClose());
    },
    onTokenAcquired: token => {
      dispatch(saveAuthToken(token));
    },
    onFormSubmitted: () => {
      dispatch(loginClearForm());
    },
    storeInitialUserData: user => {
      dispatch(storeUserData(user));
    }
  };
};

class LoginComponent extends Component {
  onSubmit = async () => {
    const {
      email,
      password,
      name,
      isRegistering,
      onError,
      onTokenAcquired,
      onFormSubmitted,
      storeInitialUserData
    } = this.props;

    if (!email || !password || (isRegistering && !name)) {
      // should we do client-side email validation?
      onError("All form fields must be filled in!");
    }

    try {
      // currently API doesn't expect more params and doesn't return token in register
      if (isRegistering) {
        const userData = await postRegister(email, password);
        if (userData) {
          const authData = await postLogin(email, password);
          // we need this for API calls as we cannot use the context from token...
          storeInitialUserData({ name: email });
          onTokenAcquired(authData.authToken);
        }
      } else {
        const authData = await postLogin(email, password);
        // we need this for API calls as we cannot use the context from token...
        storeInitialUserData({ name: email });
        onTokenAcquired(authData.authToken);
      }
      onFormSubmitted();
    } catch (err) {
      onError("Auth error!");
    }
  };

  render() {
    const {
      name,
      email,
      password,
      error,
      errorText,
      isRegistering,
      onNameChange,
      onEmailChange,
      onPasswordChange,
      onSwitchForm,
      onErrorDialogClose
    } = this.props;

    return (
      <div className="Login">
        <h2>We bet you can change your life with us!</h2>
        <div className="LoginForm">
          {isRegistering ? (
            <TextField
              value={name}
              placeholder="Name"
              onChange={e => onNameChange(e.target.value)}
            />
          ) : (
            <Spacer height={32} />
          )}
          <TextField
            value={email}
            placeholder="Email"
            onChange={e => onEmailChange(e.target.value)}
          />
          <TextField
            value={password}
            type="password"
            placeholder="Password"
            onChange={e => onPasswordChange(e.target.value)}
          />
        </div>
        <Button onClick={this.onSubmit} text="Start earning money!" />
        <Button
          variant="text"
          onClick={() => onSwitchForm()}
          text={isRegistering ? "Login" : "Register"}
        />
        <ErrorDialog
          shouldBeDisplayed={error}
          onClose={() => onErrorDialogClose()}
          titleText="Error!"
          contentText={errorText}
          buttonText="Ok"
        />
      </div>
    );
  }
}

const Login = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginComponent);

export default Login;
