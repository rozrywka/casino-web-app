import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import Button from "@material-ui/core/Button/Button";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Menu from "@material-ui/core/Menu/Menu";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Typography from "@material-ui/core/Typography/Typography";
import { connect } from "react-redux";
import {
  headerDisplayMenu,
  headerHideMenu,
  clearAuthToken
} from "../../actions";
import MenuIcon from "@material-ui/icons/Menu";
import { push } from "connected-react-router";

const mapStateToProps = state => {
  return { ...state.header };
};

const mapDispatchToProps = dispatch => {
  return {
    push: path => dispatch(push(path)),
    handleClick: anchorEl => {
      dispatch(headerDisplayMenu(anchorEl));
    },
    handleClose: () => {
      dispatch(headerHideMenu());
    },
    logout: () => {
      dispatch(clearAuthToken());
    }
  };
};

const styles = {
  flexGrow: {
    flexGrow: 1
  }
};

class HeaderComponent extends Component {
  render() {
    const {
      anchorEl,
      handleClick,
      handleClose,
      push,
      logout,
      classes
    } = this.props;

    return (
      <div className={classes.flexGrow}>
        <AppBar position="fixed">
          <Toolbar variant="dense">
            <Button
              aria-owns={anchorEl ? "simple-menu" : undefined}
              aria-haspopup="true"
              onClick={e => handleClick(e.currentTarget)}
            >
              <MenuIcon />
            </Button>
            <IconButton color="inherit" aria-label="Menu">
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={() => handleClose()}
              >
                <MenuItem onClick={() => push("/bets")}>Bet List</MenuItem>
                <MenuItem onClick={() => push("/cart")}>Cart Details</MenuItem>
                <MenuItem onClick={() => push("/user")}>User Account</MenuItem>
              </Menu>
            </IconButton>
            <Typography
              className={classes.flexGrow}
              variant="h6"
              color="inherit"
            >
              Casino App
            </Typography>
            <Button onClick={logout} color="inherit">
              Logout
            </Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const Header = withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(HeaderComponent)
);

export default Header;
