export const LOGIN_NAME_CHANGE = "LOGIN_NAME_CHANGE";
export const LOGIN_EMAIL_CHANGE = "LOGIN_EMAIL_CHANGE";
export const LOGIN_PASSWORD_CHANGE = "LOGIN_PASSWORD_CHANGE";
export const LOGIN_SWITCH_FORM = "LOGIN_SWITCH_FORM";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const LOGIN_ERROR_DIALOG_CLOSE = "LOGIN_ERROR_DIALOG_CLOSE";
export const LOGIN_CLEAR_FORM = "LOGIN_CLEAR_FORM";

export function loginNameChange(name) {
  return { type: LOGIN_NAME_CHANGE, payload: { name } };
}

export function loginEmailChange(email) {
  return { type: LOGIN_EMAIL_CHANGE, payload: { email } };
}

export function loginPasswordChange(password) {
  return { type: LOGIN_PASSWORD_CHANGE, payload: { password } };
}

export function loginSwitchForm() {
  return { type: LOGIN_SWITCH_FORM };
}

export function loginError(errorText) {
  return { type: LOGIN_ERROR, payload: { errorText } };
}

export function loginErrorDialogClose() {
  return { type: LOGIN_ERROR_DIALOG_CLOSE };
}

export function loginClearForm() {
  return { type: LOGIN_CLEAR_FORM };
}

export const SAVE_AUTH_TOKEN = "SAVE_AUTH_TOKEN";
export const CLEAR_AUTH_TOKEN = "CLEAR_AUTH_TOKEN";

export function saveAuthToken(authToken) {
  return { type: SAVE_AUTH_TOKEN, payload: { authToken } };
}

export function clearAuthToken() {
  return { type: CLEAR_AUTH_TOKEN };
}

export const HEADER_DISPLAY_MENU = "HEADER_DISPLAY_MENU";
export const HEADER_HIDE_MENU = "HEADER_HIDE_MENU";

export function headerDisplayMenu(anchorEl) {
  return { type: HEADER_DISPLAY_MENU, payload: { anchorEl } };
}

export function headerHideMenu() {
  return { type: HEADER_HIDE_MENU };
}

export const CART_ADD_ITEM = "CART_ADD_ITEM";

export function cartAddItem(id, amount) {
  return { type: CART_ADD_ITEM, payload: { id, amount } };
}

export const CART_REMOVE_ITEM = "CART_REMOVE_ITEM";

export function cartRemoveItem(id) {
  return { type: CART_REMOVE_ITEM, payload: { id } };
}

export const CART_CLEAR_ITEMS = "";

export function cartClearItems() {
  return { type: CART_CLEAR_ITEMS };
}

export const STORE_USER_DATA = "STORE_USER_DATA";

export function storeUserData(user) {
  return { type: STORE_USER_DATA, payload: { user } };
}

export const STORE_BET_LIST = "STORE_BET_LIST";

export function storeBetList(betList) {
  return { type: STORE_BET_LIST, payload: { betList } };
}
