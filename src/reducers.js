import {
  LOGIN_NAME_CHANGE,
  LOGIN_EMAIL_CHANGE,
  LOGIN_PASSWORD_CHANGE,
  LOGIN_SWITCH_FORM,
  LOGIN_ERROR,
  LOGIN_ERROR_DIALOG_CLOSE,
  LOGIN_CLEAR_FORM,
  SAVE_AUTH_TOKEN,
  CLEAR_AUTH_TOKEN,
  HEADER_DISPLAY_MENU,
  HEADER_HIDE_MENU,
  CART_ADD_ITEM,
  CART_REMOVE_ITEM,
  CART_CLEAR_ITEMS,
  STORE_USER_DATA,
  STORE_BET_LIST
} from "./actions";

import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

const initialLoginFormState = {
  name: "",
  email: "",
  password: "",
  isRegistering: false,
  error: false,
  errorText: ""
};

const initialHeaderState = {
  anchorEl: null
};

const initialBetListState = [];

const initialCartState = [];

const initialUserState = {};

function loginForm(state = initialLoginFormState, action) {
  switch (action.type) {
    case LOGIN_NAME_CHANGE:
      return { ...state, name: action.payload.name };

    case LOGIN_EMAIL_CHANGE:
      return { ...state, email: action.payload.email };

    case LOGIN_PASSWORD_CHANGE:
      return { ...state, password: action.payload.password };

    case LOGIN_SWITCH_FORM:
      return { ...state, isRegistering: !state.isRegistering };

    case LOGIN_ERROR:
      return { ...state, error: true, errorText: action.payload.errorText };

    case LOGIN_ERROR_DIALOG_CLOSE:
      return { ...state, error: false };

    case LOGIN_CLEAR_FORM:
      return { ...initialLoginFormState };

    default:
      return state;
  }
}

function header(state = initialHeaderState, action) {
  switch (action.type) {
    case HEADER_DISPLAY_MENU:
      return { ...state, anchorEl: action.payload.anchorEl };

    case HEADER_HIDE_MENU:
      return { ...state, anchorEl: null };

    default:
      return state;
  }
}

function cart(state = initialCartState, action) {
  switch (action.type) {
    case CART_ADD_ITEM: {
      const { id, amount } = action.payload;
      return [...state, { id, amount }];
    }
    case CART_REMOVE_ITEM: {
      const { id } = action.payload;
      return [...state.filter(bet => bet.id !== id)];
    }
    case CART_CLEAR_ITEMS:
      return [];
    default:
      return state;
  }
}

function betList(state = initialBetListState, action) {
  switch (action.type) {
    case STORE_BET_LIST:
      return [...action.payload.betList];
    default:
      return state;
  }
}

function user(state = initialUserState, action) {
  switch (action.type) {
    case STORE_USER_DATA:
      return {
        ...state,
        ...action.payload.user
      };
    case SAVE_AUTH_TOKEN:
      return {
        ...state,
        ...action.payload
      };
    case CLEAR_AUTH_TOKEN:
      return {
        ...initialUserState
      };
    default:
      return state;
  }
}

function buildReducer(history) {
  return combineReducers({
    router: connectRouter(history),
    loginForm,
    header,
    cart,
    betList,
    user
  });
}

export default buildReducer;
