import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { ConnectedRouter } from "connected-react-router";
import Header from "./screens/Header";
import Login from "./screens/Login";
import BetList from "./screens/BetList";
import CartDetails from "./screens/CartDetails";
import UserAccount from "./screens/UserAccount";
import { history } from "./store";

const mapStateToProps = state => {
  return { ...state.loginForm, authToken: state.user.authToken };
};

class AppComponent extends Component {
  render() {
    const { authToken } = this.props;

    if (authToken) {
      return (
        <ConnectedRouter history={history}>
          <div>
            <Header />
            <Switch>
              <Route exact path="/" render={() => <BetList />} />
              <Route exact path="/bets" render={() => <BetList />} />
              <Route exact path="/cart" render={() => <CartDetails />} />
              <Route exact path="/user" render={() => <UserAccount />} />
              <Route render={() => <div>404 Not Found</div>} />
            </Switch>
          </div>
        </ConnectedRouter>
      );
    }

    return <Login />;
  }
}

const App = connect(mapStateToProps)(AppComponent);

export default App;
