import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export default class ErrorDialog extends Component {
  render() {
    const {
      shouldBeDisplayed, // remember to change this to false in onClose
      onClose,
      titleText,
      contentText,
      buttonText
    } = this.props;
    return (
      <div>
        <Dialog open={shouldBeDisplayed} onClose={onClose}>
          <DialogTitle>{titleText}</DialogTitle>
          <DialogContent>
            <DialogContentText>{contentText}</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose} color="primary" autoFocus>
              {buttonText}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
