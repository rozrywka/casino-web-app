import React from "react";
import MaterialButton from "@material-ui/core/Button";

export default ({ onClick, text, variant = "contained" }) => (
  <MaterialButton variant={variant} color="primary" onClick={onClick}>
    {text}
  </MaterialButton>
);
