import React from "react";
import TextField from "@material-ui/core/TextField";

export default ({ value, placeholder, onChange, type }) => (
  <TextField
    value={value}
    placeholder={placeholder}
    onChange={onChange}
    type={type}
  />
);
