import { API_URL } from "../config";
import client from "./client";

export async function getUserDetails(name) {
  const url = `${API_URL}/user/name/${name}`;
  const { data } = await client.get(url);
  return data;
}

// we can also use these two functions to transfer money between users!
// simplicity wins again
export async function addMoney(login, amount) {
  const url = `${API_URL}/user/login/${login}/addMoney`;
  const { data } = await client.post(url, { amount });
  return data;
}

export async function withdrawMoney(login, amount) {
  const url = `${API_URL}/user/login/${login}/withdrawMoney`;
  const { data } = await await client.post(url, { amount });
  return data;
}
