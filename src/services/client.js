import axios from "axios";
import { store } from "../store";
import { clearAuthToken } from "../actions";

const client = () => {
  // Create instance
  let instance = axios.create();

  // Set the AUTH token for any request
  instance.interceptors.request.use(function(config) {
    const token = store.getState().user.authToken;
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  });

  instance.interceptors.response.use(
    successfulResponse => successfulResponse,
    error => {
      // if token has expired etc. then force user to relogin
      if (error.response.status === 403) {
        store.dispatch(clearAuthToken());
      }
      return Promise.reject(error);
    }
  );

  return instance;
};

export default client();
