import { API_URL } from "../config";
import client from "./client";

// currently our API accepts { name, password }, no email etc...
export async function postLogin(name, password) {
  const url = `${API_URL}/login`;
  const user = { name, password };
  const { data: authToken } = await client.post(url, user);
  // data contains the user's data, not really needed...
  return { authToken };
}

export async function postRegister(name, password) {
  const url = `${API_URL}/user/sign-up`;
  const user = {
    name,
    login: name,
    email: name,
    password,
    // it's because the user might be poor, I'm totally serious
    // not because the backend sets it to null instead of 0
    accountBalance: 10
  };
  const { data } = await client.post(url, user);
  // data contains the token as a string (not object with token field)
  return data;
}
