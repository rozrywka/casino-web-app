import { API_URL } from "../config";
import client from "./client";

export async function getBets() {
  const url = `${API_URL}/bet`;
  const { data } = await client.get(url);
  return data;
}
