import { applyMiddleware, compose, createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";
import buildReducer from "./reducers";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();

// in a true casino app we would probably use sessionStorage, not localStorage
const persistConfig = {
  key: "casinoApp",
  storage
};

const persistedReducer = persistReducer(persistConfig, buildReducer(history));

export const store = createStore(
  persistedReducer,
  {},
  composeEnhancers(applyMiddleware(routerMiddleware(history)))
);

export const persistor = persistStore(store);
