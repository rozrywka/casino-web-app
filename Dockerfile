FROM node:lts

WORKDIR /bubabet-frontend

ADD package.json package.json
RUN yarn

CMD ["yarn", "start"]
